#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "eu-vat-check",
    version = "1.0.1",
    url = 'https://github.com/ondrejsika/eu-vat-check/',
    license = 'MIT',
    description = "Check EU VAT number validity",
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    packages = find_packages(),
    include_package_data = True,
    install_requires = ["suds-jurko", ],
    zip_safe = False,
)
