#!/usr/bin/python
# coding=utf8

import suds

from conf import WDSL_ADDRESS


def eu_vat_check(checked_vat_number, requester_vat_number=None):
    """Check EU VAT number validity

    >>> eu_vat_check("CZ12345678")
    (True, None)
    >>> eu_vat_check("CZ12345678", "CZ87654321")
    (True, "WAPIAAAAUMBlhgCf")
    """

    client = suds.client.Client(WDSL_ADDRESS)

    kwargs = {
        "countryCode": checked_vat_number[:2],
        "vatNumber": checked_vat_number[2:],
    }
    if requester_vat_number:
        kwargs.update({
            "requesterCountryCode": requester_vat_number[:2],
            "requesterVatNumber": requester_vat_number[2:],
        })
    response = client.service.checkVatApprox(**kwargs)
    return response.valid, response.requestIdentifier
